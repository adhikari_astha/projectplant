# umi project

## Getting Started

Install dependencies,

```bash
$ yarn
$ npm install
```

Start the dev server,

```bash
$ yarn start
$ npm start
```
