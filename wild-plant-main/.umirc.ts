import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [{ path: '/', component: '@/pages/index' }],
  fastRefresh: {},
  // plugins: [
  //   [
  //     'umi-plugin-eslint',
  //     // eslint-loader options:
  //     {
  //       ignore: true, // 启用 .eslintignore
  //       useEslintrc: true, // 启用 .eslintrc
  //     },
  //   ],
  // ],
});
