import React, { useState } from 'react';
import { Button, Row, Layout, Col, Card, Image } from 'antd';
import Related from './Related';
import './App.css';

const Result = ({ imageUrl, onClick }) => {
  return (
    <>
      <Row gutter={16}>
        <Col span={7}>
          <Card
            width="200px"
            style={{
              boxShadow: ' 0px 0px 20px 0px gray',
            }}
            title={
              <Button type="primary" onClick={onClick}>
                이미지 다시 선택하기
              </Button>
            }
            style={{
              width: '300px',
              height: '300px',
              boxShadow: ' 0px 0px 20px 0px gray',
            }}
          >
            <Image width={200} height={200} src={imageUrl}></Image>
          </Card>
        </Col>
        <Col span={7}>
          <Card
            title="식물 이미지"
            style={{
              width: '300px',
              height: '330px',
              boxShadow: ' 0px 0px 20px 0px gray',
            }}
          >
            <Image height={200} src="https://ifh.cc/g/YbTFm6.jpg" />
          </Card>
        </Col>
        <Col span={10}>
          <Card
            title="성분분석 이미지"
            style={{
              boxShadow: ' 0px 0px 20px 0px gray',
              width: '100%',
              height: '100%',
            }}
          >
            <Image height={200} src="https://ifh.cc/g/bTYZS8.png" />
          </Card>
        </Col>
      </Row>
      <Related />
    </>
  );
};

export default Result;
