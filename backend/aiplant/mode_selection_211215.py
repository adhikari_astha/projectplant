from .auto_classification_211215 import auto_model
from .out_classification_211215 import output_model


# image = "E:/Datasets/wild_plants/public_dataset/3class_dataset/test/flower/image_00095.jpg"

# image = "E:/Datasets/wild_plants/public_dataset/3class_dataset/test/leaf/1082.jpg"

# image = "E:/Datasets/wild_plants/public_dataset/flower_dataset/test/26/image_06864.jpg"

# image = "E:/Datasets/wild_plants/public_dataset/fruit_dataset/test/17/f81_17_81.jpg"

# image = "E:/Datasets/wild_plants/public_dataset/leaf_dataset/test/9/3120.jpg"

# flower : value == 0, fruit : value == 1, leaf-front : value == 2, leaf-back : value == 3

# value = 2

def main(image, value):

# def main():
        # print("image :", image)
        # print("value :", value)
        print("<<<<< start_main >>>>>")

        if value < 4 :
                return output_model(image, value) # manual mode (In this case, an input image is moved directly to part_model.)
        else :
                return auto_model(image) # Activating a model which can classify the part of plants ex) flower, fruit, leaf-front, leaf-back
                

if __name__ == '__main__':
        main()