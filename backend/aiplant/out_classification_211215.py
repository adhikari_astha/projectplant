# from customdataset import CustomImageDataset
# from beautifultable import BeautifulTable
import copy
import time
import torch
import numpy as np
from torch import nn
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import torchvision.models as models
from .VGG_211217 import VGG
# from VGG_model_for_train import VGG_2node
# from scale_distance_of_features import scale_of_feature

import matplotlib.pyplot as plt
# from confusion_matrix_210825 import confusion_matrix
from PIL import Image
import cv2


print("<<<<< test start >>>>>")


hyper_param_epoch = 1
hyper_param_batch = 50
hyper_param_learning_rate = 0.001
model_number = 2
model_name = ['VGG11', 'VGG13', 'VGG16', 'VGG19']

model_epoch = 100



def classname_list(value) :


    if value == 0 : # flower
        temp_class_name = ['90', '83', '25', '68', '47', '52', '64', '23', '42', '70', '94', '65', '8', '100', '38',
                            '98', '91', '14', '81', '67', '55', '16', '24', '19', '28', '21', '6', '88', '89', '82',
                            '22', '93', '0', '84', '41', '62', '18', '17', '51', '76', '97', '12', '43', '48', '37',
                            '92', '101', '78', '60', '3', '10', '27', '57', '35', '73', '45', '61', '40', '26', '39',
                            '85', '77', '46', '30', '74', '44', '54', '13', '71', '31', '75', '96', '7', '87', '63',
                            '59', '34', '29', '9', '11', '2', '66', '1', '86', '72', '36', '53', '5', '58', '15',
                            '20', '79', '33', '99', '69', '50', '80', '95', '32', '49', '56', '4']

    elif value == 1 : # fruit

        temp_class_name = ['90', '83', '25', '68', '47', '52', '64', '23', '110', '42', '70', '94', '65', '8', '100', '116',
                       '121','38', '98', '91', '14', '81', '67', '130', '104', '55', '16', '118', '24', '108', '111','129',
                       '19', '28','21', '6', '88', '89', '82', '102', '22', '93', '0', '84', '41', '62', '18', '107', '17',
                       '51', '76', '97', '12', '43', '48', '37', '92', '101', '78', '60', '3', '10', '122', '27', '57', '125',
                       '35', '109', '73', '45', '61', '40', '26', '39', '85', '77', '46', '30', '74', '106', '44', '54', '13',
                       '71', '31', '120', '75', '96', '7', '87', '63', '59', '126', '34', '103', '29', '105', '9', '115', '112',
                       '11', '2', '124', '66', '1', '86', '72', '36', '53', '5', '58', '119', '128', '15', '123', '117', '20',
                       '79','33', '127','99', '69', '50', '80', '95', '32', '49', '56', '114', '113', '4']

    else : # leaf
        temp_class_name = ['0', '1', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '2', '20', '21', '22',
                           '23', '24', '25', '26', '27', '28', '29', '3', '30', '31', '4', '5', '6', '7', '8', '9']
    return temp_class_name

def image_loader(image_path, loader): # single image load
    """load image, returns cuda tensor"""
    # image_name = image_path()
    # uploaded_image = Image.open(image_path)
    # uploaded_image = loader(uploaded_image).float()
    # uploaded_image = uploaded_image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
    # image_original= torch.tensor(np.array(image_path))
    # print(image_original.shape)
    # image_original = torch.permute(image_original,(2,0,1))
    # uploaded_image = image_original.float().unsqueeze(0)  #this is for VGG, may not be needed for ResNet
    # return uploaded_image.cuda() #assumes that you're using GPU

    trans = transforms.ToPILImage()

    
    uploaded_image = torch.tensor(image_path)

    uploaded_image = torch.permute(uploaded_image,(2,0,1))
    
    uploaded_image = trans(uploaded_image)
   
    uploaded_image = loader(uploaded_image).float()
   
    uploaded_image = uploaded_image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
  
    return uploaded_image.cuda()



def output_model(image, class_number):
    # print("model_image_path :", image)
    # print("class_number :", class_number)

    loader = transforms.Compose([transforms.Resize((100, 100)), transforms.ToTensor(),
                                 transforms.Normalize(mean=[0.4914, 0.4822, 0.4465],
                                                      std=[0.2023, 0.1994, 0.2010])])
    input_image = image_loader(image, loader)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    class_names = classname_list(class_number)
    class_dict ={}

    # 나중에 이미지마다 모델이 각기 다를 수 있기 때문에 구분하는 부분 나중에 추가하기 ex) fullshot은 vgg이고 flower는 densenet인 경우
    if class_number == 0 : # model for flower image

        num_classes = 102
        model_path = './aiplant/weights/flower_weight_211220/vgg16_bn(pretrained)/train_weight_25_(VGG16)_best.pth'

        wildplant_model = models.vgg16_bn(pretrained=True)
        in_features = wildplant_model.classifier[6].in_features
        wildplant_model.classifier[6] = nn.Linear(in_features, num_classes)
        class_dict['class']='flower'

        print("<<<<< loading flower model >>>>>>")

    elif class_number == 1 : # model for fruit image

        num_classes = 131
        model_path = './aiplant/weights/fruit_weight_211221/vgg16_bn(scratch)/train_weight_172_(VGG16)_best.pth'

        wildplant_model = VGG(m_name=model_name[model_number], num_classes=num_classes)
        class_dict['class']='fruit'

        print("<<<<< loading fruit model >>>>>>")


    elif class_number == 2: # model for leaf(front) image

        num_classes = 32
        model_path = './aiplant/weights/leaf_weight_211221/vgg19_bn(pretrained)/train_weight_19_(VGG19)_best.pth'
        wildplant_model = models.vgg19_bn(pretrained=True)

        in_features = wildplant_model.classifier[6].in_features
        wildplant_model.classifier[6] = nn.Linear(in_features, num_classes)
        class_dict['class']='leaf'

        print("<<<<< loading leaf(front) model >>>>>>")

    else : # model for leaf(back) image (추후에 변경 예정)

        num_classes = 32
        model_path = './aiplant/weights/leaf_weight_211221/vgg19_bn(pretrained)/train_weight_19_(VGG19)_best.pth'

        wildplant_model = models.vgg19_bn(pretrained=True)
        in_features = wildplant_model.classifier[6].in_features
        wildplant_model.classifier[6] = nn.Linear(in_features, num_classes)

        print("<<<<< loading leaf(back) model >>>>>>")



    wildplant_model.load_state_dict(torch.load(model_path))  # pretrained model load



    wildplant_model.to(device)



    wildplant_model.eval()

    with torch.no_grad():
        outputs = wildplant_model(input_image)

        # print("outputs :", len(outputs[0]))
        # print("outputs :",outputs)
        # print("#### shape: ", outputs.shape)


        _, predicted_eval = torch.max(outputs.data, 1)
        class_value = class_names[predicted_eval]
        class_dict['value']=class_value
        print("predicted_eval(class name)_out classification :", class_value)
        return class_dict
        # print("predicted_class_number :", int(class_names[predicted_eval]))
        # print("predicted_class :", class_names[int(predicted_eval)])

        # 관련 사진 랜덤으로 5장, 성분 파일, 관련 설명 TEXT 파일 웹으로 출력하는 코드 추가하기



    print("<<<<< completed >>>>>")

