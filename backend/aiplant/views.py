from django.shortcuts import render
import json
from django.shortcuts import render
from django.template import loader
import numpy as np
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import base64
from PIL import Image
import io
from .mode_selection_211215 import main

def decodeImage(data):
    #Gives us 1d array
    decoded = np.fromstring(data, dtype=np.uint8)
    return decoded

@csrf_exempt
def Imageget(request):
    if request.method == 'POST':
        if(request):
            myfile = request.FILES['image'] 
            select_option=request.POST['aivalue']   
            with myfile.open("rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
            base64_decoded = base64.b64decode(encoded_string)
            image = Image.open(io.BytesIO(base64_decoded))
            image_np = np.array(image)
            print('I am inside Imageget',image_np.shape)
            print(image_np.shape)
            print('select-option:',select_option)
            out=main(image_np,int(select_option))
            print('I am in out...the final output',out)

        else:
            print(request.FILES)
       
    return HttpResponse(json.dumps({'data': out}), content_type="application/json")
    # return HttpResponse(json.dumps({'data': {'one':'two'}}), content_type="application/json")