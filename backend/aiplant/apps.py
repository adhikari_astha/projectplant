from django.apps import AppConfig


class AiplantConfig(AppConfig):
    name = 'aiplant'
