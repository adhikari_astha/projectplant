# from customdataset import CustomImageDataset
# from beautifultable import BeautifulTable
import copy
import time
import torch
import numpy as np
from torch import nn
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from .VGG_211217 import VGG
# from utils_for_wildplant_210827 import visualize_features
# from scale_distance_of_features import scale_of_feature

import matplotlib.pyplot as plt
# from confusion_matrix_210825 import confusion_matrix
from PIL import Image

from .out_classification_211215 import output_model


print("<<<<< webservice Start >>>>>")

class_names = ['flower','fruit','leaf']
# class_names = ['flower','fruit','leaf-front','leaf-back']

num_classes = len(class_names)

hyper_param_epoch = 1
hyper_param_batch = 50
hyper_param_learning_rate = 0.001
model_number = 0
model_name = ['VGG16', 'VGG19']


#image_path는 웹에서 업로드된 이미지를 특정 폴더에 저장한 후에 그 이미지를 불러오는 것으로 하기
#웹서비스를 통해 저장된 이미지는 자동으로 1시간이내에 삭제되도록 코드 추가하기

model_path = './aiplant/weights/3class_weight_211220/vgg16_bn(scratch)/train_weight_40_(VGG16)_best.pth'


def image_loader(image_path, loader): # single image load
    """load image, returns cuda tensor"""
   
    # image_original= torch.tensor(np.array(image_path))
   
    # image_original = loader(image_original)
   
    # image_original = torch.permute(image_original,(2,0,1))
    
    # uploaded_image = image_original.float().unsqueeze(0)  #this is for VGG, may not be needed for ResNet

    # return uploaded_image.cuda()


    trans = transforms.ToPILImage()

    uploaded_image = torch.tensor(image_path)
   
    uploaded_image = torch.permute(uploaded_image,(2,0,1))

    uploaded_image = trans(uploaded_image)
 
    uploaded_image = loader(uploaded_image).float()
 
    uploaded_image = uploaded_image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
    
    return uploaded_image.cuda()



def auto_model(image):

    # print("auto_model :",image)

    loader = transforms.Compose([transforms.Resize((100,100)), transforms.ToTensor(),
                                 transforms.Normalize(mean=[0.4914, 0.4822, 0.4465],
                                                    std=[0.2023, 0.1994, 0.2010])])
    input_image = image_loader(image, loader)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


    print("num_classes :", num_classes)



    wildplant_model = VGG(m_name=model_name[model_number], num_classes=num_classes)

    # print("hidden_model :",hidden_model)

    wildplant_model.load_state_dict(torch.load(model_path))  # pretrained model load

    wildplant_model.to(device)


    wildplant_model.eval()

    with torch.no_grad():


        hidden_outputs = wildplant_model(input_image)
        # print("hidden_features :",hidden_features)
        # print("hidden_outputs :",hidden_outputs)
        # print("hidden_outputs len :", len(hidden_outputs[0]))

        _, predicted_eval = torch.max(hidden_outputs.data, 1)

        class_name = class_names[predicted_eval]
        print("predicted_eval(class name)_auto classification :",class_name)
        # print("predicted_class :", int(class_names[predicted_eval]))
        # print("predicted_class :", class_names[int(class_names[predicted_eval])])
        # print(image_path)

        # print(aaa)

        class_dict=output_model(image, int(predicted_eval))
        class_dict['class'] = class_name
        return class_dict

